const { PORT } = require('./src/config')
const { conexion } = require('./src/database')
const app = require('./src/config/app')

// Conexión a la base de datos
conexion()

// Servidor
app.listen(PORT, ()=>{
  console.log(`Servidor funcionando en el puerto ${PORT}`)
})