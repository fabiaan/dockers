const { Router } = require('express')
const { userController } = require('../controllers')
const router = Router()

router.get('/', userController.get)

router.post('/', userController.create)

router.get('/:userId', userController.getById)

router.put('/:userId', userController.update)

router.delete('/:userId', userController.delete)

router.post('/verify', userController.getByEmail)

module.exports = router