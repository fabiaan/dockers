module.exports = {
  userRoutes: require('./user.routes'),
  subjectRoutes: require('./subject.routes'),
  tutoringRoutes: require('./tutoring.routes')
}