const { User } = require('../models')

module.exports = async function validateTeacher(id){
  try{
    const query = { _id: id }
    const proyection = { _id: 1, role: 1 }

    const result = await User.findOne(query, proyection)

    if(result.role === 'profesor'){
      return id
    }
  } catch(e){
    const error = new Error()
    error.status = 400
    error.message = `El id ${id} no pertenece a un profesor`
    throw error
  }
}