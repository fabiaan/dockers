const mongoose = require('mongoose')
const { Schema } = mongoose

const SubjectSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  class: {
    type: String,
    require: true
  },
  teacher: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
    autopopulate: {
      select: "name lastname email"
    }
  },
  students: [
    {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: false,
      autopopulate: true
    }
  ]
},{
  versionKey: false
})

SubjectSchema.plugin(require('mongoose-autopopulate'))
module.exports = mongoose.model('subject', SubjectSchema)