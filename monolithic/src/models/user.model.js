const mongoose = require('mongoose')
const { Schema } = mongoose

const {
  hashSync,
  compareSync,
  genSaltSync
} = require('bcryptjs')

const UserSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  lastname: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  role: {
    type: String,
    require: true,
    enum: {
      values: ['profesor', 'estudiante'],
      message: '{VALUE} no es un rol válido'
    }
  }
},{
  versionKey: false
})

UserSchema.methods.toJSON = function(){
  let user = this.toObject()
  delete user.password
  return user
}

UserSchema.statics.comparePassword = async (password, receivedPassword)=>{
  return await compareSync(password, receivedPassword)
}

UserSchema.pre('save', async function(next){

  const user = this
  const salt = genSaltSync(10)

  const hashedPassword = hashSync(user.password, salt)
  user.password = hashedPassword

  next()
})

module.exports = mongoose.model('user', UserSchema)