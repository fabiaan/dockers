const { User } = require('../models')

module.exports = verifyEmail = async (req, res, next)=>{

  // Verificar email
  const email = await User.findOne({email: req.body.email})
  if(email){
    return res
      .status(400)
      .json({message: 'Este email ya está registrado'})
  }
  next()
}
