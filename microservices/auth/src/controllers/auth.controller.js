const jwt = require('jsonwebtoken')
const { SECRET } = require('../config')
const { User } = require('../models')

const auth = {}

auth.singIn = async(req, res)=>{
  try {
    const {body} = req
    // generar token
    const token = jwt.sign({id: req.body._id}, SECRET, {
      expiresIn: 86400
    })

    res.json({user: body, token})
  } catch (e) {
    res
      .status(500)
      .json({message: 'Hable con un administrador'})
  }
}

auth.singUp = async (req, res)=>{

  const { body } = req

  try {
    // Create user
    const user = await User.create(body)

    // token
    const token = jwt.sign({id: user._id}, SECRET, {
      expiresIn: 86400 // One day
    })
    res.json({
      user: user,
      token: token
    })
  } catch (e) {
    console.error(e)
  }
}

module.exports = auth