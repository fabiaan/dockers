const { Router } = require('express')
const { authController } = require('../controllers')
const { verifyEmail } = require('../middlewares')

const router = Router()

router.post('/signin', authController.singIn)
router.post('/signup', verifyEmail, authController.singUp)

module.exports = router